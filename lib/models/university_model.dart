class UnivertyModel {
  int? id;
  String? univName;
  String? fullUnivName;
  String? logo;
  String? city;
  int? votes;

  UnivertyModel(
      {this.id,
      this.univName,
      this.fullUnivName,
      this.logo,
      this.city,
      this.votes});

  UnivertyModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    univName = json['univ_name'];
    fullUnivName = json['full_univ_name'];
    logo = json['logo'];
    city = json['city'];
    votes = json['votes'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['univ_name'] = univName;
    data['full_univ_name'] = fullUnivName;
    data['logo'] = logo;
    data['city'] = city;
    data['votes'] = votes;
    return data;
  }
}
