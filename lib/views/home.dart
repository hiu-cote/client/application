import 'package:cote_app/models/university_model.dart';
import 'package:cote_app/utils/custom_behavior.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';

import '../utils/mediaquery.dart';


class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int crossAxisCountAdaptative() {
    if (_size.width(context) > 1366) {
      return 6;
    } else if (_size.width(context) <= 1115 && _size.width(context) >= 1045) {
      return 4;
    } else if (_size.width(context) < 1045 && _size.width(context) > 786) {
      return 3;
    } else if (_size.width(context) < 786 && _size.width(context) > 746) {
      return 2;
    } else if (_size.width(context) < 746) {
      return 1;
    }
    return 5;
  }

  List<UnivertyModel> univs = [
    UnivertyModel(
      univName: 'ITU',
      fullUnivName: 'IT University -Université spécialisée en Informatique',
      city: 'Tananarive',
      votes: 50,
      logo:
          'https://www.ituniversity-mg.com/page/wp-content/uploads/2021/08/ITU_logo_MAJ_negatif_sans_fond.png',
    ),
  ];
  final CustomSize _size = CustomSize();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('HIU COTE APP'),
      ),
      body: ScrollConfiguration(
        behavior: MyCustomScrollBehavior(),
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(8.0),
            height: _size.height(context),
            width: _size.width(context),
            // implement GridView.builder
            child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: crossAxisCountAdaptative(),
                  childAspectRatio: 2.5,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  mainAxisExtent: 80,
                ),
                itemCount: 100,
                itemBuilder: (BuildContext ctx, index) {
                  UnivertyModel univ = univs[0];

                  return universityCard(
                    name: univ.univName!,
                    fullName: univ.fullUnivName!,
                    city: univ.city!,
                    vote: univ.votes!,
                    logo: univ.logo!,
                  );
                }),
          ),
        ),
      ),
    );
  }

  Widget universityCard({
    required String name,
    required String fullName,
    required String city,
    required int vote,
    required String logo,
  }) {
    return Container(
      width: _size.width(context),
      height: _size.height(context),
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: BoxDecoration(
        color: Colors.teal,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CircleAvatar(
            radius: 40,
            backgroundImage: NetworkImage(logo),
          ),
          SizedBox(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  name,
                  maxLines: 1,
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 13.0,
                    color: Colors.white,
                  ),
                ),
                SizedBox(
                  height: 22,
                  width: 150,
                  child: Text(
                    fullName,
                    maxLines: 2,
                    textAlign: TextAlign.start,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 10.0,
                      color: Colors.white,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  city,
                  maxLines: 1,
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 10.0,
                    color: Colors.white,
                  ),
                ),
                LinearPercentIndicator(
                  barRadius: const Radius.circular(15),
                  padding: const EdgeInsets.only(right: 5),
                  animation:
                      true, //animate when it shows progress indicator first
                  percent:
                      60 / 100, //vercentage value: 0.6 for 60% (60/100 = 0.6)
                  progressColor: Colors.white,
                  width: 100,
                  trailing: Text(
                    '$vote%',
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 10.0,
                      color: Colors.white,
                    ),
                  ),
                  //350 - 440
                  /*center: Text(
                    '$vote%',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 10.0,
                      color: Colors.white,
                    ),
                  ), //center text, you can set Icon as well*/

                  //
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget universityCard2({
    required String name,
    required String fullName,
    required String city,
    required int vote,
    required String logo,
  }) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.teal,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: _size.width(context) >= 277
                ? _size.height(context) * .15
                : _size.height(context) * .1,
            width: _size.width(context),
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(15),
                  topRight: Radius.circular(15),
                ),
                image: DecorationImage(
                    fit: BoxFit.cover, image: NetworkImage(logo))),
            child: Stack(
              alignment: AlignmentDirectional.center,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(.5),
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15),
                    ),
                  ),
                ),
                SizedBox(
                  child: CircularPercentIndicator(
                    radius: 30, //radius for circle
                    lineWidth: 8.0, //width of circle line
                    animation:
                        true, //animate when it shows progress indicator first
                    percent:
                        60 / 100, //vercentage value: 0.6 for 60% (60/100 = 0.6)
                    progressColor: Colors.white,

                    circularStrokeCap: CircularStrokeCap.round,
                    //350 - 440
                    center: Text(
                      '$vote%',
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 10.0,
                        color: Colors.white,
                      ),
                    ), //center text, you can set Icon as well

                    footer: !((_size.width(context) >= 350 &&
                                _size.width(context) <= 440) ||
                            _size.width(context) <= 300)
                        ? Text(
                            name,
                            style: const TextStyle(
                              fontWeight: FontWeight.normal,
                              fontSize: 17.0,
                              color: Colors.white,
                            ),
                          )
                        : const SizedBox(), //
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 30,
                  child: Text(
                    fullName,
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 10.0,
                      color: Colors.white,
                    ),
                  ),
                ),
                !((_size.width(context) >= 350 &&
                            _size.width(context) <= 440) ||
                        _size.width(context) <= 300)
                    ? Text(
                        '- $city -',
                        maxLines: 1,
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 10.0,
                          color: Colors.white,
                        ),
                      )
                    : const SizedBox()
              ],
            ),
          )
        ],
      ),
    );
  }
}
